function updatePrice() {
    let s = document.querySelectorAll("select");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    let radioDiv = document.querySelector(".radios");
    radioDiv.style.display = (select.value == "2" ? "flex" : "none");
    
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        if (radio.checked) {
            let optionPrice = prices.prodOptions[radio.value];
            if (optionPrice !== undefined) {
                price = prices.prodTypes[priceIndex];
                price += optionPrice;
            }
        }
    });

    let chekdiv = document.querySelector(".checkboxes");
    chekdiv.style.display = (select.value == "3" ? "flex" : "none");

    let checkboxes = document.querySelectorAll(".checkboxes input");
    checkboxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price = prices.prodTypes[priceIndex];
                price += propPrice;
            }
        }
    });

    let result_out = document.querySelector(".result");
    result_out.innerHTML ="Стоимость товара : " + price + " рублей";
    return price;
}

function getPrices() {
    return {
        prodTypes: [1490, 3499, 9850],
        prodOptions: {
            option1: 300,
            option2: 500,
            option3: 910,
        },
        prodProperties: {
            prop1: 400,
            prop2: 990,
        }
    };
}

function onClick() {
    let quantity = document.querySelector("#quantity");
    if (!Number.isNaN(parseInt(quantity.value))) {
        let result = document.querySelector(".result");
        result.innerHTML = ("Общая стоимость : " +
            parseInt(quantity.value) * updatePrice() + " Рублей ! ");
    } else
        alert("Введены некоректные символы. Заполните форму заново!");

}

window.addEventListener("DOMContentLoaded", function (event) {
    let radioDiv = document.querySelector(".radios");
    radioDiv.style.display = "none";

    let s = document.getElementsByName("select");
    let select = s[0];
    select.addEventListener("change", updatePrice);

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", updatePrice);
    });

    let checkboxes = document.querySelectorAll(".checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", updatePrice);
    });
 
    updatePrice();
    
    let btn = document.querySelector(".myBtn");
    btn.addEventListener("click", onClick);
});
